/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : student_test

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-07-17 11:04:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for examroom
-- ----------------------------
DROP TABLE IF EXISTS `examroom`;
CREATE TABLE `examroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `desction` varchar(255) DEFAULT NULL COMMENT '考试场次描述',
  `paper_id` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of examroom
-- ----------------------------
INSERT INTO `examroom` VALUES ('1', '六级在线考试', '2018年6月英语六级真题：CET6题目', '1,2,3', '2018-07-12 15:30:46', '2018-07-12 15:30:49');
INSERT INTO `examroom` VALUES ('3', '四级在线考试', '2018年6月英语四级真题：CET4题目', '2,5,7', '2018-07-15 16:36:59', '2018-07-15 16:36:59');
INSERT INTO `examroom` VALUES ('4', '专业八级在线考试', '2018年6月英语专业八级真题：TEM-8题目', '4,2,7', '2018-07-16 10:58:44', '2018-07-16 10:58:44');

-- ----------------------------
-- Table structure for paper
-- ----------------------------
DROP TABLE IF EXISTS `paper`;
CREATE TABLE `paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `choice_id_list` varchar(255) DEFAULT NULL,
  `judge_id_list` varchar(255) DEFAULT NULL,
  `short_ans_id_list` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paper
-- ----------------------------
INSERT INTO `paper` VALUES ('1', '1,2,3,4', null, '5,6', '2018-07-12 15:29:47', '2018-07-12 15:29:50');
INSERT INTO `paper` VALUES ('2', '4,7,2,8', null, '9,5', '2018-07-15 16:00:28', '2018-07-15 16:00:28');
INSERT INTO `paper` VALUES ('3', '3,2,7,8', null, '16,6', '2018-07-15 16:00:46', '2018-07-15 16:00:46');
INSERT INTO `paper` VALUES ('4', '12,14,3,2', null, '15,16', '2018-07-15 16:01:01', '2018-07-15 16:01:01');
INSERT INTO `paper` VALUES ('5', '2,7,8,14', null, '16,5', '2018-07-15 16:01:48', '2018-07-15 16:01:48');
INSERT INTO `paper` VALUES ('6', '7,12,11,1', null, '16,5', '2018-07-15 16:02:03', '2018-07-15 16:02:03');
INSERT INTO `paper` VALUES ('7', '11,8,1,3', null, '16,17', '2018-07-15 16:33:54', '2018-07-15 16:33:54');
INSERT INTO `paper` VALUES ('8', '8,13,2,12', null, '9,16', '2018-07-15 16:36:59', '2018-07-15 16:36:59');
INSERT INTO `paper` VALUES ('9', '1,12,11,8', null, '9,16', '2018-07-16 10:58:44', '2018-07-16 10:58:44');

-- ----------------------------
-- Table structure for problem
-- ----------------------------
DROP TABLE IF EXISTS `problem`;
CREATE TABLE `problem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '题目标题',
  `desction` varchar(255) DEFAULT NULL COMMENT '题目描述',
  `answer` varchar(255) DEFAULT NULL COMMENT '题目答案',
  `right_ans` varchar(255) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of problem
-- ----------------------------
INSERT INTO `problem` VALUES ('1', '选择题', 'He looks good ____red while black looks good _____me.', 'A．in ; on B．on; in C．in; in	D．on; on', 'A', '10', '2018-07-12 15:17:25', '2018-07-12 15:17:28');
INSERT INTO `problem` VALUES ('2', '选择题', 'Thank you for your help .---____.', 'A．That’s right B．Don’t mention it C．All right D．You are right', 'B', '10', '2018-07-12 15:19:44', '2018-07-12 15:19:46');
INSERT INTO `problem` VALUES ('3', '选择题', '---Have you seen ____ pen ? I left it here this morning .\r\n---Is it _____ black one ? I think I saw it somewhere.', 'A．a; a B．a; the C．the; a D．the; the ', 'A', '10', '2018-07-12 15:26:16', '2018-07-12 15:26:18');
INSERT INTO `problem` VALUES ('4', '选择题', 'It is _____ good film that none of us dislike it .', 'A．a so B．so a C．a such D．such a', 'D', '10', '2018-07-12 15:27:07', '2018-07-12 15:27:10');
INSERT INTO `problem` VALUES ('5', '简答题', 'If you lose 12 times in a row (连续地), will you just give up? Dale Carnegie, a pioneer in public speaking and self-development, kept trying. And he became famous by showing people how to be successful.', null, null, '30', '2018-07-12 15:28:21', '2018-07-12 15:28:24');
INSERT INTO `problem` VALUES ('6', '简答题', 'When Carnegie first entered college, he felt upset because of his ragged (破旧的) clothes and ordinary(平凡的) looks. His mother encouraged him, “Why not try to be better in other things instead of just dress and good looks?”', null, null, '30', '2018-07-12 15:29:06', '2018-07-12 15:29:08');
INSERT INTO `problem` VALUES ('7', '选择题', 'Agriculture is the country’s chief source of wealth, wheat ____ by far the biggest cereal crop.', 'A. is B. been C. be D. being', 'D', '10', '2018-07-14 11:34:23', '2018-07-14 11:34:23');
INSERT INTO `problem` VALUES ('8', '选择题', 'Jack ____from home for two days now, and I am beginning to worry about his safety.', 'A. has been missing B. has been missed?\r\nC. had been missing D. was missed', 'A', '10', '2018-07-14 11:35:32', '2018-07-14 11:35:32');
INSERT INTO `problem` VALUES ('9', '简答题', 'Above the trees are the hills, ____ magnificence the river faithfully reflects on the surface.', null, null, '30', '2018-07-14 11:36:00', '2018-07-14 11:36:00');
INSERT INTO `problem` VALUES ('10', '选择题', 'The opening ceremony is a great occasion. It is essential ____for that.', 'A. for us to be prepared B. that we are prepared C. of us to be prepared D. our being prepared', 'A', '10', '2018-07-15 11:20:21', '2018-07-15 11:20:21');
INSERT INTO `problem` VALUES ('11', '选择题', 'Time ____, the celebration will be held as scheduled.', 'A. permit B. permitting C. permitted D. permits', 'B', '10', '2018-07-15 11:58:37', '2018-07-15 11:58:37');
INSERT INTO `problem` VALUES ('12', '选择题', 'They overcame all the difficulties and completed the project two months ahead of time, ____ is something we had not expected.', 'A. which B. it C. that D. what', 'A', '10', '2018-07-15 11:59:10', '2018-07-15 11:59:10');
INSERT INTO `problem` VALUES ('13', '选择题', 'He is quite worn out from years of hard work. He is not the man ____ he was twenty years ago.', 'A. which B. that C. who D. whom', 'B', '10', '2018-07-15 11:59:27', '2018-07-15 11:59:27');
INSERT INTO `problem` VALUES ('14', '选择题', 'It will take us twenty minutes to get to the railway station, ____traffic delays.', 'A. acknowledging B. affording C. allowing for D. accounting for', 'C', '10', '2018-07-15 12:00:24', '2018-07-15 12:00:24');
INSERT INTO `problem` VALUES ('15', '简答题', 'Mr. Brown‟s condition looks very serious and it is doubtful if he will ?', null, null, '30', '2018-07-15 12:01:15', '2018-07-15 12:01:15');
INSERT INTO `problem` VALUES ('16', '简答题', 'The majority of nurses are women, but in the higher ranks of the medical profession women are in a ?', null, null, '30', '2018-07-15 12:01:35', '2018-07-15 12:01:35');
INSERT INTO `problem` VALUES ('17', '简答题', 'Nancy\'s gone to work but her car\'s still there. Why she by bus?', null, null, '30', '2018-07-15 12:02:13', '2018-07-15 12:02:13');

-- ----------------------------
-- Table structure for solution
-- ----------------------------
DROP TABLE IF EXISTS `solution`;
CREATE TABLE `solution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) DEFAULT NULL,
  `paper_id` int(11) DEFAULT NULL,
  `choice_ans` varchar(255) DEFAULT NULL,
  `judge_ans` varchar(255) DEFAULT NULL,
  `short_answer_ans` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `total` int(11) unsigned zerofill NOT NULL DEFAULT '00000000000',
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of solution
-- ----------------------------
INSERT INTO `solution` VALUES ('4', '201512030230', '1', 'A@B@C@D', null, '答案一@答案二', 'yes', '00000000020', '2018-07-13 00:32:46', '2018-07-15 17:12:03');
INSERT INTO `solution` VALUES ('10', '201512030231', '1', 'A@B@C@D', null, '答案一@答案二', 'yes', '00000000090', '2018-07-14 14:58:45', '2018-07-14 20:58:54');
INSERT INTO `solution` VALUES ('13', '201512030230', '2', 'A@A@B@B', null, '我的答案一@我的答案二', 'yes', '00000000020', '2018-07-15 17:00:17', '2018-07-15 17:12:03');
INSERT INTO `solution` VALUES ('24', '201512030233', '4', '@@@', null, '@', null, '00000000000', '2018-07-17 10:44:47', '2018-07-17 10:44:47');
INSERT INTO `solution` VALUES ('25', '201512030233', '5', 'A@A@@', null, '@', null, '00000000000', '2018-07-17 10:48:10', '2018-07-17 10:48:10');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) NOT NULL COMMENT '用户账号',
  `passwd` varchar(255) NOT NULL COMMENT '用户密码',
  `user_role` varchar(255) NOT NULL COMMENT '角色',
  `exam_id` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '201512030230', '12345678', 'student', '1,3', '2018-07-11 18:55:55', '2018-07-11 18:55:55');
INSERT INTO `user` VALUES ('2', '201512030231', '12345678', 'student', '1', '2018-07-12 12:04:12', '2018-07-12 12:04:18');
INSERT INTO `user` VALUES ('3', '201512030232', '12345678', 'student', '1', '2018-07-12 12:04:15', '2018-07-12 12:04:21');
INSERT INTO `user` VALUES ('4', 'admin', '123', 'teacher', '', '2018-07-12 12:04:02', '2018-07-12 12:04:06');
INSERT INTO `user` VALUES ('7', '201512030124', '12345678', 'student', '', '2018-07-16 11:00:52', '2018-07-16 11:00:52');
INSERT INTO `user` VALUES ('8', '201512030233', '12345678', 'student', '3,4', null, '2018-07-17 10:48:10');
